public class Main {
    public static void main(String[] args) {
        User user1 = new User("Jun", "Shiomi", 34, "Japan");

        Course course1 = new Course();

        course1.setName("Spices 123");
        course1.setDescription("Advance Spice Mixing");
        course1.setSeats(15);
        course1.setFee(12999.00);
        course1.setStartDate("25-04-2022");
        course1.setEndDate("29-04-2022");
        course1.setInstructor(user1);


        System.out.println("User's first name:");
        System.out.println(user1.getFirstName());
        System.out.println("User's last name:");
        System.out.println(user1.getLastName());
        System.out.println("User's age:");
        System.out.println(user1.getAge());
        System.out.println("User's address:");
        System.out.println(user1.getAddress());
        System.out.println("Course's name:");
        System.out.println(course1.getName());
        System.out.println("Course's description:");
        System.out.println(course1.getDescription());
        System.out.println("Course's seats:");
        System.out.println(course1.getSeats());
        System.out.println("Course's instructor's first name:");
        System.out.println(course1.getInstructorName());
    }
}